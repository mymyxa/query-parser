package request

type Edge struct {
	From string `json:"from"`
	To   string `json:"to"`
}
