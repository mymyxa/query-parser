package request

import (
	"encoding/json"
	"fmt"
)

type Transformable interface {
	Apply(fields []string, from string) string
}

type Node struct {
	Key             string        `json:"key"`
	Type            NodeType      `json:"type"`
	TransformObject Transformable `json:"transformObject"`
}

func (n *Node) Apply(fields []string, from string, with bool) string {
	if with {
		return fmt.Sprintf("WITH %s as (\n\t\t%s\n\t)", n.Key, n.TransformObject.Apply(fields, from))
	}

	return fmt.Sprintf(" %s as (\n\t\t%s\n\t  )", n.Key, n.TransformObject.Apply(fields, from))
}

func (n *Node) UnmarshalJSON(data []byte) error {
	var tmp struct {
		Key             string          `json:"key"`
		Type            NodeType        `json:"type"`
		TransformObject json.RawMessage `json:"transformObject"`
	}

	err := json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}

	n.Type = tmp.Type
	n.Key = tmp.Key

	switch tmp.Type {
	case Input:
		n.TransformObject = new(NodeInput)
	case Filter:
		n.TransformObject = new(NodeFilter)
	case Sort:
		v := new(NodeSort)
		err = json.Unmarshal(tmp.TransformObject, &v.nodes)
		n.TransformObject = v
		return err
	case TextTransformation:
		v := new(NodeTextTransformation)
		err = json.Unmarshal(tmp.TransformObject, &v.nodes)
		n.TransformObject = v
		return err
	case Output:
		n.TransformObject = new(NodeOutput)
	default:
		return fmt.Errorf("unsupported NodeType %s", tmp.Type)
	}

	return json.Unmarshal(tmp.TransformObject, &n.TransformObject)
}
