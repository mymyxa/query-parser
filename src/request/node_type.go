package request

import (
	"fmt"
	"strings"
)

type NodeType string

const (
	Input              NodeType = "INPUT"
	Filter             NodeType = "FILTER"
	Sort               NodeType = "SORT"
	TextTransformation NodeType = "TEXT_TRANSFORMATION"
	Output             NodeType = "OUTPUT"
)

type NodeInput struct {
	TableName string   `json:"tableName"`
	Fields    []string `json:"fields"`
}

func (n *NodeInput) Apply(fields []string, from string) string {
	return fmt.Sprintf("SELECT `%s` FROM `%s`", strings.Join(n.Fields, "`, `"), n.TableName)
}

type NodeFilter struct {
	VariableFieldName string       `json:"variable_field_name"`
	JoinOperator      string       `json:"joinOperator"`
	Operations        []*Operation `json:"operations"`
}

func (n *NodeFilter) Apply(fields []string, from string) string {
	operations := make([]string, 0)
	for _, o := range n.Operations {
		operations = append(operations, fmt.Sprintf("`%s` %s", n.VariableFieldName, o.String()))
	}

	return fmt.Sprintf("%s WHERE %s", selectString(fields, from), strings.Join(operations, n.JoinOperator))
}

type Operation struct {
	Operator string `json:"operator"`
	Value    string `json:"value"`
}

func (o *Operation) String() string {
	return fmt.Sprintf("%s %s", o.Operator, o.Value)
}

type NodeSort struct {
	nodes []NodeSortElem
}

func (n *NodeSort) Apply(fields []string, from string) string {
	nodes := make([]string, 0)
	for _, node := range n.nodes {
		nodes = append(nodes, node.String())
	}
	return fmt.Sprintf("%s ORDER BY %s", selectString(fields, from), strings.Join(nodes, ", "))
}

type NodeSortElem struct {
	Target string `json:"target"`
	Order  string `json:"order"`
}

func (n *NodeSortElem) String() string {
	return fmt.Sprintf("`%s` %s", n.Target, n.Order)
}

type NodeTextTransformation struct {
	nodes []NodeTextTransformationElem
}

func (n *NodeTextTransformation) Apply(fields []string, from string) string {
	transformationsMap := make(map[string]NodeTextTransformationElem)
	for _, node := range n.nodes {
		transformationsMap[node.Column] = node
	}

	result := make([]string, 0)
	for _, field := range fields {
		if transformation, exists := transformationsMap[field]; exists {
			result = append(result, transformation.String())
		} else {
			result = append(result, fmt.Sprintf("`%s`", field))
		}
	}
	return fmt.Sprintf("SELECT %s FROM `%s`", strings.Join(result, ", "), from)
}

type NodeTextTransformationElem struct {
	Column         string `json:"column"`
	Transformation string `json:"transformation"`
}

func (n *NodeTextTransformationElem) String() string {
	return fmt.Sprintf("%s(`%s`) as `%s`", n.Transformation, n.Column, n.Column)
}

type NodeOutput struct {
	Limit  int `json:"limit"`
	Offset int `json:"offset"`
}

func (n *NodeOutput) Apply(fields []string, from string) string {
	return fmt.Sprintf("%s LIMIT %d OFFSET %d", selectString(fields, from), n.Limit, n.Offset)
}

func selectString(fields []string, from string) string {
	fieldsStr := "*"
	if len(fields) > 0 {
		fieldsStr = "`" + strings.Join(fields, "`, `") + "`"
	}
	return fmt.Sprintf("SELECT %s FROM `%s`", fieldsStr, from)
}
