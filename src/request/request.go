package request

type Request struct {
	Nodes []*Node `json:"nodes"`
	Edges []*Edge `json:"edges"`
}
