package request

import (
	"encoding/json"
	"strings"
	"testing"
)

func TestParser_Parse(t *testing.T) {
	var request Request
	err := json.Unmarshal(GetRequestData(), &request)
	if err != nil {
		t.Errorf("Request unmarshal error %s", err)
	}

	parser := new(Parser)
	result, err := parser.Parse(&request)
	if err != nil {
		t.Errorf("Request parsing error %s", err)
	}

	expectedResult := "WITH A as (\n\t\tSELECT `id`, `name`, `age` FROM `users`\n\t),\n B as (\n\t\tSELECT `id`, `name`, `age` FROM `A` WHERE `age` > 18\n\t  ),\n C as (\n\t\tSELECT `id`, `name`, `age` FROM `B` ORDER BY `age` ASC, `name` ASC\n\t  ),\n D as (\n\t\tSELECT `id`, UPPER(`name`) as `name`, `age` FROM `C`\n\t  ),\n E as (\n\t\tSELECT * FROM `D` LIMIT 100 OFFSET 0\n\t  )\nSELECT * FROM E;\n"
	if strings.Compare(expectedResult, result) != 0 {
		t.Errorf("Request parsing result:\n%s\n is not as expected:\n%s", result, expectedResult)
	}
}

func GetRequestData() []byte {
	return []byte(`{
    "nodes": [
        {
            "key": "A",
            "type": "INPUT",
            "transformObject": {
                "tableName": "users",
                "fields": [
                    "id",
                    "name",
                    "age"
                ]
            }
        },
        {
            "key": "B",
            "type": "FILTER",
            "transformObject": {
                "variable_field_name": "age",
                "joinOperator": "AND",
                "operations": [
                    {
                        "operator": ">",
                        "value": "18"
                    }
                ]
            }
        },
        {
            "key": "C",
            "type": "SORT",
            "transformObject": [
                {
                    "target": "age",
                    "order": "ASC"
                },
                {
                    "target": "name",
                    "order": "ASC"
                }
            ]
        },
        {
            "key": "D",
            "type": "TEXT_TRANSFORMATION",
            "transformObject": [
                {
                    "column": "name",
                    "transformation": "UPPER"
                }
            ]
        },
        {
            "key": "E",
            "type": "OUTPUT",
            "transformObject": {
                "limit": 100,
                "offset": 0
            }
        }
    ],
    "edges": [
        {
            "from": "A",
            "to": "B"
        },
        {
            "from": "B",
            "to": "C"
        },
        {
            "from": "C",
            "to": "D"
        },
        {
            "from": "D",
            "to": "E"
        }
    ]
}
	`)
}
