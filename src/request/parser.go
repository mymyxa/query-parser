package request

import (
	"errors"
	"fmt"
	"strings"
)

type Parser struct {
}

func (p *Parser) Parse(r *Request) (string, error) {
	if len(r.Nodes) < 1 {
		return "", errors.New("request parsing error: count of nodes is low")
	}
	inputNode := r.Nodes[0]
	if strings.Compare(string(inputNode.Type), string(Input)) != 0 {
		return "", errors.New("request parsing error: input node is required")
	}

	fields := (inputNode.TransformObject).(*NodeInput).Fields

	edgesMapToFrom := make(map[string]string)
	edgesMapFromTo := make(map[string]string)

	for _, e := range r.Edges {
		edgesMapToFrom[e.To] = e.From
		edgesMapFromTo[e.From] = e.To
	}

	queries := make([]string, 0)
	for _, node := range r.Nodes {
		edge := node.Key

		from, exists := edgesMapToFrom[edge]
		if !exists {
			queries = append(queries, node.Apply(fields, from, true))
		} else {
			if _, ok := edgesMapFromTo[edge]; ok {
				queries = append(queries, node.Apply(fields, from, false))
			} else {
				queries = append(queries, node.Apply(nil, from, false))
				return fmt.Sprintf("%s\nSELECT * FROM %s;\n", strings.Join(queries, ",\n"), edge), nil
			}
		}
	}

	return "", nil
}
