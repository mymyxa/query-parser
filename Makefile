project = query-parser

build:
	docker build -t ${project} -f docker/Dockerfile .

run: build
	docker run -it --rm --name ${project} ${project} ./${project}

run-test:
	docker run -it --rm --name ${project} ${project} go test ./...

run-test-cover:
	docker run -it --rm --name ${project} ${project} go test ./... --cover

.PHONY: build run run-test run-test-cover