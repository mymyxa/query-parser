package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/mymyxa/query-parser/src/request"
	"io/ioutil"
)

func main() {
	data, err := ioutil.ReadFile("data/request-data.json")
	if err != nil {
		fmt.Printf("Error reading file %v", err)
	}

	var parsedData request.Request
	err = json.Unmarshal(data, &parsedData)
	if err != nil {
		fmt.Printf("Request unmarshal error %s", err)
	}

	parser := new(request.Parser)
	result, err := parser.Parse(&parsedData)
	if err != nil {
		fmt.Printf("Request parse error %s", err)
	}

	fmt.Println(result)
}
